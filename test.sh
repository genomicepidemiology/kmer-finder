#!/usr/bin/env bash
#

mkdir tmp
# use FASTA file input
echo '####################'
echo 'use FASTA file input'
echo '####################'
maketemplatedb -i data/all.fsa -x ATGAC -o tmp/test_DB_fsa
echo '####################'
# use list input
echo '##############'
echo 'use list input'
echo '##############'
maketemplatedb -l data/test_list.txt -x ATGAC -o tmp/test_DB_list
echo '--------------'
use a homology threshold
echo '########################'
echo 'use a homology threshold'
echo '########################'
maketemplatedb -l data/test_list.txt -t 0.8 -x ATGAC -o tmp/test_out_h
echo '########################'
#use organism list
echo '#################'
echo 'use organism list'
echo '#################'
maketemplatedb -i data/artificial.fsa -c data/artificial_organisms.txt -x ATGAC -o tmp/test_DB_artificial_organism
echo '#################'

# create a kmer-database :
echo '#################'
echo 'create a kmer-database '
echo '#################'
maketemplatedb -i data/artificial.fsa -c data/artificial_organisms.txt -x ATGAC -o tmp/test_DB_artificial
# create a kmer-database using the organism list :
echo '#################'
echo 'create a kmer-database using the organism list'
echo '#################'
maketemplatedb -i data/artificial.fsa -c data/artificial_organisms.txt -x ATGAC -o tmp/test_DB_artificial_organism
# print information about kmer-database :
echo '#################'
echo 'print information about kmer-database'
echo '#################'
readPrintKmerDB -t tmp/test_DB_artificial -l -u -i -d
#print information about organism kmer-database :
echo '#################'
echo 'print information about organism kmer-database'
echo '#################'
readPrintKmerDB -t tmp/test_DB_artificial_organism -l -u -i -d

# use organism list :
echo '#################'
echo 'use organism list'
echo '#################'
# FIXME: Fix missing organism key (look into error)
makeorganismdb -i data/artificial_organisms.txt -t tmp/test_DB_artificial -o tmp/test_DB_artificial_organism2


# findtemplate.py with the "winner takes it all" scoring scheme:
echo '#################'
echo 'findtemplate.py with the "winner takes it all" scoring scheme'
echo '#################'
# FIXME: Fix global names
findtemplate -i data/chimeric_seq.fsa -t tmp/test_DB_fsa -o tmp/test_out.txt -x ATGAC -w
# findtemplate.py with the "standard" scoring scheme:
echo '#################'
echo 'findtemplate.py with the "standard" scoring scheme'
echo '#################'
# FIXME: Fix global names
findtemplate -i data/chimeric_seq.fsa -t tmp/test_DB_fsa -o tmp/test_out_standard.txt -x ATGAC

# TODO: test makeTree
# TODO: test getTax
# TODO: test createTable
#
rm -r tmp
