install:
	python setup.py install
distribute:
	python setup.py sdist
clean:
	find ./ -name "*.pyc" -delete
	find ./ -name "*.DS_Store" -delete
newenv:
	virtualenv env
	echo 'RUN: source env/bin/activate'
test:
	bash test.sh
